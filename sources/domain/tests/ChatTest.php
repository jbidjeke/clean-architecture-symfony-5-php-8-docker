<?php

namespace JBIDJEKE\Project\Domain\Test;

use PHPUnit\Framework\TestCase;
use JBIDJEKE\Project\Domain\UseCase\Chat;
use JBIDJEKE\Project\Domain\Entity\Message;
use JBIDJEKE\Project\Domain\Response\ChatResponse;
use JBIDJEKE\Project\Domain\Gateway\MessageGateway;
use JBIDJEKE\Project\Domain\Presenter\ChatPresenterInterface;

/**
 * Message Class
 * @package JBIDJEKE\Project\Test
 */

class ChatTest extends TestCase
{
    /**
     *
     * @var MessageGateway
     */
    private MessageGateway $messageGateway;
    private ChatPresenterInterface $presenter;

    protected  function setUp(): void 
    {
        parent::setUp();

        $this->messageGateway = new class() implements MessageGateway {

            /**
             *
             * @param Message $message
             * @return void
             */
            public function add(Message $message): void
            {

            }

            /**
             *
             * @return array
             */
            public function findAll(): array 
            {
                return array_fill(0, 10, new Message('Message'));
            }
        };

        $this->presenter = new class() implements ChatPresenterInterface {

            public array $messages;

            /**
             *
             * @param ChatResponse $chatResponse
             */
            public function present(ChatResponse $chatResponse)
            {
                $this->messages = array_map(
                    fn(Message $message) => $message->getContent(), 
                    $chatResponse->getMessages()
                );

            }
        };
    }

    public function test(): void
    {
        (new Chat($this->messageGateway))->execute($this->presenter);
        $this->assertCount(10, $this->presenter->messages);
    }
}
