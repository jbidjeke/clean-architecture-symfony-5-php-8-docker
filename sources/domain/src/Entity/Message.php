<?php

namespace JBIDJEKE\Project\Domain\Entity;


/**
 * Message Class
 * @package JBIDJEKE\Project\Domain\Entity
 */
class Message
{
    /**
     *
     * @var string
     */
    private string $content;


    public function __construct($content)
    {
        $this->content = $content;
    }



    /**
     * Get the value of content
     *
     * @return  string
     */ 
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of content
     *
     * @param  string  $content
     *
     * @return  self
     */ 
    public function setContent(string $content)
    {
        $this->content = $content;

        return $this;
    }
}
