<?php

namespace JBIDJEKE\Project\Domain\Request;

/**
 * Class SendRequest
 * @package JBIDJEKE\Project\Domain\Request
 */
class SendRequest
{
    protected string $message;

    /**
     *
     * @param string $message
     */
    public function __construct(string $message)
    {
        $this->message = $message;
    }


    /**
     * Get the value of message
     */ 
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set the value of message
     *
     * @return  self
     */ 
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }
}
