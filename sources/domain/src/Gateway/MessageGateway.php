<?php

namespace JBIDJEKE\Project\Domain\Gateway;

use JBIDJEKE\Project\Domain\Entity\Message;

/**
 * Data access Interface
 * @package JBIDJEKE\Project\Domain\Gateway
 */
interface MessageGateway
{
    /**
     *
     * @param Message $message
     * @return void
     */
    public function add(Message $message): void;

    /**
     *
     * @return array
     */
    public function findAll(): array;

}
