<?php

namespace JBIDJEKE\Project\Domain\Response;

/**
 * Class ChatResponse
 * @package JBIDJEKE\Project\Domain\Response
 */
class ChatResponse
{
    /**
     *
     * @var Message[]
     */
    private array $messages = [];


    /**
     * ChatResponse constructor
     *
     * @param Message[]
     */
    public function __construct(array $messages)
    {
        $this->messages = $messages;
    }


    /**
     * Get the value of messages
     *
     * @return  Message[]
     */ 
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set the value of messages
     *
     * @param  Message[]
     *
     * @return  self
     */ 
    public function setMessages(array $messages)
    {
        $this->messages = $messages;

        return $this;
    }
}
