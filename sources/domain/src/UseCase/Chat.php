<?php

namespace JBIDJEKE\Project\Domain\UseCase;

use JBIDJEKE\Project\Domain\Presenter\ChatPresenterInterface;
use JBIDJEKE\Project\Domain\Response\ChatResponse;
use JBIDJEKE\Project\Domain\Entity\Message;
use JBIDJEKE\Project\Domain\Gateway\MessageGateway;

/**
 * Class Chat
 * @package JBIDJEKE\Project\Domain\UseCase
 */
class Chat
{

    public function __construct(private MessageGateway $messageGateway){}


    /**
     *
     * @param ChatPresenterInterface $presenter
     * @return void
     */
    public function execute(ChatPresenterInterface $presenter): void 
    {
        $chatResponse = new ChatResponse($this->messageGateway->findAll());
        $presenter->present($chatResponse);
    }

}
