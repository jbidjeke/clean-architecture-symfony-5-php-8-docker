<?php

namespace JBIDJEKE\Project\Domain\UseCase;

use JBIDJEKE\Project\Domain\Entity\Message;
use JBIDJEKE\Project\Domain\Gateway\MessageGateway;
use JBIDJEKE\Project\Domain\Request\SendRequest;

/**
 * Class Send
 * @package JBIDJEKE\Project\Domain\UseCase
 */
class Send
{
    /**
     *
     * @var MessageGateway
     */
    private MessageGateway $messageGateway;

    public function __construct(MessageGateway $messageGateway)
    {
        $this->messageGateway = $messageGateway;
    }


    /**
     *
     * @param SendRequest $request
     * @return void
     */
    public function execute(SendRequest $request): void 
    {
        $message = new Message($request->getMessage());
        
        $this->messageGateway->add($message);

    }
}
