<?php

namespace JBIDJEKE\Project\Domain\Presenter;

use JBIDJEKE\Project\Domain\Response\ChatResponse;

/**
 * ChatPresenter Interface 
 * @package JBIDJEKE\Project\Domain\Presenter
 */
interface  ChatPresenterInterface
{
    /**
     *
     * @param ChatResponse $chatResponse
     */
    public function present(ChatResponse $chatResponse);

}
