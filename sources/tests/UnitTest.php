<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Demo;

class UnitTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }

    public function testDemo() 
    {
        $demo = new Demo();
        $demo->setId(1);
        $this->assertTrue($demo->getId() === 1);
    }
}
