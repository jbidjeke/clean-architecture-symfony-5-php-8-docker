<?php

namespace App\UserInterface\Controller;

use JBIDJEKE\Project\Domain\Request\SendRequest;
use JBIDJEKE\Project\Domain\UseCase\Send;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ChatController
 * @package  App\UserInterface\Controller
 */
class SendController
{

    /**
     *
     * @param Send $send
     * @param Request $request
     * @return void
     */
    public function __invoke(Send $send, Request $request): JsonResponse
    {
       $request = new SendRequest($request->getContent());
       $send->execute($request);

       return new JsonResponse(null, Response::HTTP_CREATED);
    }
}
