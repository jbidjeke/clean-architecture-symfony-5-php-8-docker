<?php

namespace App\UserInterface\Controller;

use JBIDJEKE\Project\Domain\UseCase\Chat;
use App\UserInterface\Presenter\ChatPresenter;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ChatController
 * @package  App\UserInterface\Controller
 */
class ChatController
{

    /**
     *
     * @param Chat $chat
     * @return JsonResponse
     */
    public function __invoke(Chat $chat): JsonResponse
    {
        $presenter = new ChatPresenter();

        $chat->execute($presenter); // use case Chat

        return new JsonResponse($presenter->getChatViewModel());
        
    }
}
