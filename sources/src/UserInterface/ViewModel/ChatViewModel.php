<?php

namespace App\UserInterface\ViewModel;

/**
 * Class Chat
 * @package  App\UserInterface\ViewModel
 */
class ChatViewModel
{

    /**
     *
     * @param array|string[] $messages
     */
    public function __construct(public array $messages){}


    /**
     * Get the value of messages
     *
     * @return  array
     */ 
    public function getMessages(): array
    {
        return $this->messages;
    }


}
