<?php

namespace App\UserInterface\Presenter;


use JBIDJEKE\Project\Domain\Entity\Message;
use App\UserInterface\ViewModel\ChatViewModel;
use JBIDJEKE\Project\Domain\Response\ChatResponse;
use JBIDJEKE\Project\Domain\Presenter\ChatPresenterInterface;

/**
 * Class ChatPresenter
 * @package  App\UserInterface\Presenter
 */
class ChatPresenter implements ChatPresenterInterface
{

    public function __construct(private ?ChatViewModel $chatViewModel = null){}

    /**
     *
     * @param ChatResponse $chatResponse
     */
    public function present(ChatResponse $chatResponse)
    {
        $this->chatViewModel = new ChatViewModel(
            array_map(
                fn(Message $message) => $message->getContent(), 
                $chatResponse->getMessages()
            )
        );

    }

    public function  getChatViewModel(): ChatViewModel {
        return $this->chatViewModel;
    }

}
