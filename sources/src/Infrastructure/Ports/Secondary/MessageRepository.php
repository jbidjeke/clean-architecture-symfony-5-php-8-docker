<?php

namespace App\Infrastructure\Ports\Secondary;

use JBIDJEKE\Project\Domain\Entity\Message;
use JBIDJEKE\Project\Domain\Gateway\MessageGateway;

/**
 * Class MessageRepository
 * @package  App\Infrastructure\Ports\Secondary
 */
class MessageRepository implements MessageGateway
{
    /**
     *
     * @param Message $message
     * @return void
     */
    public function add(Message $message): void
    {

    }

    /**
     *
     * @return array
     */
    public function findAll(): array 
    {
        return array_fill(0, 10, new Message('Message'));
    }

}
