# "Clean Architecture" Symfony 5 PHP 8 Docker

En quelques décennies, nous assistons à l'évolution exponentielle des technologies logicielles avec une série de modèles d'architecture de systèmes. 

"Clean Architecture" est une philosophie de conception logicielle qui sépare les éléments d'une conception en niveaux d'anneaux. Un objectif important de l'architecture propre est de fournir aux développeurs un moyen d'organiser le code de manière à encapsuler la logique métier tout en la séparant du mécanisme de livraison.


## Caractéristiques de la "Clean Architecture"

### Indépendance du châssis (frameworks)

L'architecture ne dépend pas de l'existence d'une riche bibliothèque de fonctions. Les frameworks doivent être vus comme des outils, ne vous obligeant pas à adapter votre projet à leurs contraintes et limites.

### Indépendance de l'interface utilisateur

L'interface utilisateur peut être modifiée facilement sans affecter le reste du système. Il est possible de remplacer une interface Web UI par une interface console sans impact sur la règle métier

### Indépendance de la base de données

Vous devriez pouvoir remplacer Oracle ou SQL Server par Mongo, BigTable, CouchDB ou autre. Les règles métier ne sont pas liées à la base de données.

### Indépendance de tout équipement externe

Vos règles métier doivent être libres de tout lien avec des interfaces vers l'extérieur.

### Testabilité

Il est essentiel que les règles métier puissent être testées sans la présence d'éléments externes tels que l'interface utilisateur, la base de données, le serveur, etc.
[exemples](https://gitlab.com/jbidjeke/clean-architecture-symfony-5-php-8-docker/-/blob/main/sources/domain/tests/ChatTest.php)



### Règles de dépendance

Les dépendances de code source ne doivent être  orientées que vers l'intérieur, vers les règles de haut niveau
[examples](https://gitlab.com/jbidjeke/clean-architecture-symfony-5-php-8-docker/-/blob/main/sources/src/UserInterface/Presenter/ChatPresenter.php)


## Avantage

Des cas d'utilisation facilement identifiables et clairement énoncés ;
Facile à déployer les entités indépendamment et à les réutiliser dans plusieurs applications d'entreprise ;
Plus facile de comprendre rapidement le cas d'utilisation.


## Naturals Risks, à eviter lors de l'implémentation

Domaine moins structuré et plus « vide » ;
Réutiliser la logique pour plusieurs cas d'utilisation plus complexes (ou dangereux) ;
Danger de tomber dans un domaine anémique ou de faire de la programmation procédurale sans le vouloir ;
Danger de concevoir des entités par les données plutôt que d'avoir un domaine cohérent qui représente bien la logique du domaine métier ;
Moins d'outils concernant comment réussir à modéliser son domaine, avoir un langage commun, etc. ;
Risque plus élevé de complexité dans "l'Interacteur", plutôt que d'abstraire cette complexité dans la conception du domaine.

# Conclusion

Le secret pour construire un grand projet facile à maintenir et plus performant est de séparer les fichiers et les classes en composants qui peuvent changer indépendamment sans affecter les autres composants : c'est ce qu'est la Clean Architecture.
